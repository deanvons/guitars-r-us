import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import appMiddleware from "./Middleware/appMiddleware";
import appReducer from "./Reducers/appReducers";

export default createStore(appReducer, composeWithDevTools(appMiddleware));
