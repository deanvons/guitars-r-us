import { ACTION_USER_ADD_DEFAULT, ACTION_USER_SET  } from "../Actions/userActions";

export function userReducer(state = {}, action) {
  switch (action.type) {
    case ACTION_USER_ADD_DEFAULT:
      return { ...state, userName: "defaultUser", email: "default email" };
    case ACTION_USER_SET:
      return { ...action.payload };
    default:
      return state;
  }
}
