import { combineReducers } from "redux";
import { cartReducer } from "./cartReducer";
import { userReducer } from "./userReducer";

const appReducer = combineReducers({user:userReducer,cart:cartReducer});

export default appReducer;
