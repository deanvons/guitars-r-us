import { ACTION_CART_SET } from "../Actions/cartActions";

export function cartReducer(state = {items:[]}, action) {
  switch (action.type) {
    case ACTION_CART_SET:
      return { ...state, items: [...state.items,action.payload] };
    default:
      return state;
  }
}
