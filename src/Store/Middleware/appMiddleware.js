import { applyMiddleware } from "redux";
import { userMiddleware } from "./userMiddleWare";

export default applyMiddleware(userMiddleware);
