import {
  ACTION_LOAD_USER,
  ACTION_USER_ADD_DEFAULT,
  actionSetUser,
} from "../Actions/userActions";

export const userMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_LOAD_USER) {
      fetch("https://randomuser.me/api/")
        .then((response) => response.json())
        .then((result) => dispatch(actionSetUser(result.results[0])));
    }

    
  };
