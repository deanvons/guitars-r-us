// TYPES LABEL ACTIONS (quality of life convention)
export const ACTION_USER_ADD_DEFAULT = "[user] ADD-DEFAULT-USER";
export const ACTION_USER_SET = "[user] SET-USER";
export const ACTION_LOAD_USER = "[user] LOAD-USER"

// ACTION METHODS

export function actionAddDefaultUser() {
  return { type: ACTION_USER_ADD_DEFAULT };
}

// can update the user
export function actionSetUser(user) {
  return { type: ACTION_USER_SET, payload:user };
}

export function actionLoadUser() {
  return { type: ACTION_LOAD_USER };
}