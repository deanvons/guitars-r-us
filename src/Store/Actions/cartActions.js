// TYPES LABEL ACTIONS (quality of life convention)
export const ACTION_CART_SET = "[cart] SET-CART";


// ACTION METHODS

export function actionSetCart(item) {
  return { type: ACTION_CART_SET, payload:item };
}
