import { useContext } from 'react'
import { useSelector } from 'react-redux'
import { cartReducer } from '../../Store/Reducers/cartReducer'
import {UserContext} from '../Context/UserContext'
import GuitarListItem from '../Guitar/GutiarListItem'


function Profile(){

    const user = useSelector(state => state.user)
    const guitars = useSelector(state => state.cart.items)

    
  let guitarList = guitars.map((sampleGuitar) => (
    <GuitarListItem
      key={sampleGuitar.id}
      guitar={sampleGuitar}
    />
  ));

    return(
        <>
            <h4>Profile Info</h4>
            {guitarList}
        </>
    )
}

export default Profile