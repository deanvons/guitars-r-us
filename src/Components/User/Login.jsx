import { useContext, useEffect, useState } from "react";
import { UserContext } from "../Context/UserContext";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { actionAddDefaultUser, actionLoadUser, actionSetUser } from "../../Store/Actions/userActions";

function Login() {
  // this will go in state
  const user = useSelector(state => state.user);
  const navigate = useNavigate();
  const dispatch = useDispatch()


  function loadUser() {
   
    // test our new action method
    dispatch(actionLoadUser())
    navigate('/guitars')
  }

  return (
    <>
      <button onClick={loadUser}>Load User</button>
      <h4>{user.userName}</h4>
    </>
  );
}

export default Login;
