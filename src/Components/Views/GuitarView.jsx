import GuitarList from "../Guitar/GuitarList";

function GuitarView() {
  return (
    <>
      <h1>Guitar page</h1>
      <GuitarList />
    </>
  );
}

export default GuitarView;
