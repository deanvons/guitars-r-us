import GuitarDetails from "../Guitar/GuitarDetails";

function GuitarDetailView() {
  return (
    <>
      <h1>Guitar detail page</h1>
      <GuitarDetails />
    </>
  );
}

export default GuitarDetailView;
