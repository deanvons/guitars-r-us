function GuitarListItem({ guitar,onGuitarSelected }) {

function clickGuitar(){
    onGuitarSelected(guitar.id)
}


  return (
    <>
      <div onClick={clickGuitar}>
        <h4>{guitar.model}</h4>
        <img src={guitar.image} alt="guitar pic" />
      </div>
    </>
  );
}

export default GuitarListItem