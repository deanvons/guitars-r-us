import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { actionSetCart } from "../../Store/Actions/cartActions";

function GuitarDetails() {
  const [guitar, setGuitar] = useState({});
  const { guitarId } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    fetch(
      `https://noroff-assignment-users-api.herokuapp.com/guitars/${guitarId}`
    )
      .then((response) => response.json())
      .then((result) => setGuitar(result));
  }, []);

  function addToCart() {
dispatch(actionSetCart(guitar))

  }

  return (
    <>
      {guitar.materials != undefined && (
        <div>
          <h4>{guitar.model}</h4>
          <h4>Materials</h4>
          <p>{guitar.materials.neck}</p>
          <p>{guitar.materials.fretboard}</p>
          <p>{guitar.materials.body}</p>
          <img src={guitar.image} alt="guitar pic" className="guitar" />
          <button onClick={addToCart}>Add to Cart</button>
        </div>
      )}
    </>
  );
}

export default GuitarDetails;
