import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import UserAvatar from "../User/UserAvatar";
import GuitarListItem from "./GutiarListItem";

function GuitarList() {
  const [guitars, setGuitars] = useState([]);
  const user = useSelector((state) => state.user);
  const navigate = useNavigate();

  function handleGuitarSelected(guitarId) {
    navigate(`/guitar/${guitarId}`);
  }

  let guitarList = guitars.map((sampleGuitar) => (
    <GuitarListItem
      key={sampleGuitar.id}
      guitar={sampleGuitar}
      onGuitarSelected={handleGuitarSelected}
    />
  ));

  useEffect(() => {
    loadGuitar();
  }, []);

  function loadGuitar() {
    fetch("https://noroff-assignment-users-api.herokuapp.com/guitars")
      .then((response) => response.json())
      .then((result) => setGuitars(result));
  }

  return (
    <div>
      <h1>Guitar List</h1>
      {guitarList.length > 0 && <div>{guitarList}</div>}
    </div>
  );
}

export default GuitarList;
