import logo from "./logo.svg";
import "./App.css";
import GuitarView from "./Components/Views/GuitarView";
import UserCard from "./Components/HOC/UserCard";
import UserInfo from "./Components/HOC/UserInfo";
import UserPurchases from "./Components/HOC/UserPurchases";
import { BrowserRouter, NavLink, Route, Routes } from "react-router-dom";
import HomeView from "./Components/Views/HomeView";
import GuitarDetailView from "./Components/Views/GuitarDetailView";
import GuitarList from "./Components/Guitar/GuitarList";
import UserProfileView from "./Components/Views/UserProfileView";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <NavLink to="/">Home</NavLink>
        <NavLink to="/guitars">Guitars</NavLink>
        <NavLink to="/guitar">Guitar</NavLink>
        <NavLink to="/profile">User Profile</NavLink>
        <Routes>
          <Route path="/" element={<HomeView />} />
          <Route path="/guitars" element={<GuitarView />} />
          <Route path="/guitar/:guitarId" element={<GuitarDetailView />} />
          <Route path="/profile" element={<UserProfileView />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
